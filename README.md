# object-copy [![NPM version](https://img.shields.io/npm/v/object-copy.svg?style=flat)](https://www.npmjs.com/package/object-copy) [![NPM downloads](https://img.shields.io/npm/dm/object-copy.svg?style=flat)](https://npmjs.org/package/object-copy) [![Build Status](https://img.shields.io/travis/jonschlinkert/object-copy.svg?style=flat)](https://travis-ci.org/jonschlinkert/object-copy)

Copy static properties, prototype properties, and descriptors from one object to another.

## Install

Install with [npm](https://www.npmjs.com/):

```sh
$ npm install object-copy --save
```

## Usage

```js
var copy = require('object-copy');
```

## API

### [copy](index.js#L26)

Copy static properties, prototype properties, and descriptors from one object to another.

**Params**

* `receiver` **{Object}**
* `provider` **{Object}**
* `omit` **{String|Array}**: One or more properties to omit
* `returns` **{Object}**

**Example**

```js
function App() {}
var proto = App.prototype;
App.prototype.set = function() {};
App.prototype.get = function() {};

var obj = {};
copy(obj, proto);
```

## Contributing

This document was generated by [verb-readme-generator][] (a [verb](https://github.com/verbose/verb) generator), please don't edit directly. Any changes to the readme must be made in [.verb.md](.verb.md). See [Building Docs](#building-docs).

Pull requests and stars are always welcome. For bugs and feature requests, [please create an issue](../../issues/new). Or visit the [verb-readme-generator][] project to submit bug reports or pull requests for the readme layout template.

## Building docs

Generate readme and API documentation with [verb](https://github.com/verbose/verb):

```sh
$ npm install -g verb verb-readme-generator && verb
```

## Running tests

Install dev dependencies:

```sh
$ npm install -d && npm test
```

## Author

**Jon Schlinkert**

* [github/jonschlinkert](https://github.com/jonschlinkert)
* [twitter/jonschlinkert](http://twitter.com/jonschlinkert)

## License

Copyright © 2016, [Jon Schlinkert](https://github.com/jonschlinkert).
Released under the [MIT license](https://github.com/jonschlinkert/object-copy/blob/master/LICENSE).

***

_This file was generated by [verb](https://github.com/verbose/verb), v0.9.0, on June 09, 2016._